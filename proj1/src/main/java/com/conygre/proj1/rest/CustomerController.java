package com.conygre.proj1.rest;

import com.conygre.proj1.entities.Customer;
import com.conygre.proj1.repos.CustomerRepository;
import com.conygre.proj1.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/customers")
@CrossOrigin
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @PostMapping
    public Customer addNewCustomer(@RequestBody Customer newCustomer) {
        return customerService.addNewCustomer(newCustomer);
    }

    @GetMapping
    public Collection<Customer> getCustomers() {
        return customerService.getCustomers();
    }

    @GetMapping("/{customerId}")
    public Customer getCustomerById(@PathVariable("customerId") int id) {
        return customerService.getCustomerById( id);
    }

    @PutMapping
    public Customer updateCustomer(@RequestBody Customer updatedCustomer) {
        return customerService.updateCustomer(updatedCustomer);
    }

    @DeleteMapping("/{customerId}")
    public void deleteCustomer(@PathVariable("customerId") int id) {
        customerService.deleteCustomer(id);
    }

    @DeleteMapping
    public void deleteCustomer(@RequestBody Customer customer) {
        customerService.deleteCustomer(customer);
    }
}
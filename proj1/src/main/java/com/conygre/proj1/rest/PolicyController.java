package com.conygre.proj1.rest;

import com.conygre.proj1.entities.Policy;
import com.conygre.proj1.services.CustomerService;
import com.conygre.proj1.services.PolicyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/policies")
@CrossOrigin
public class PolicyController {

    @Autowired
    private PolicyService policyService;

    @Autowired
    private CustomerService customerService;

    @PostMapping
    public Policy addNewPolicy(@RequestBody Policy newPolicy) {

        return policyService.addNewPolicy(newPolicy);
    }

    @GetMapping
    public Collection<Policy> getPolicies() {
        return policyService.getPolicies();
    }

    @GetMapping("/{policyId}")
    public Policy getPolicyById(@PathVariable("policyId") int id) {
        return policyService.getPolicyById(id);
    }

    @PutMapping
    public Policy updatePolicy(@RequestBody Policy updatedPolicy) {
        return policyService.updatePolicy(updatedPolicy);
    }

    @DeleteMapping("/{policyId}")
    public void deletePolicy(@PathVariable("policyId") int id) {
        policyService.deletePolicy(id);
    }

    @DeleteMapping
    public void deletePolicy(@RequestBody Policy policy) {
        policyService.deletePolicy(policy);
    }
}

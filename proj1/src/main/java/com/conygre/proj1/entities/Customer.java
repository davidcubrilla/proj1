package com.conygre.proj1.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="customers")

public class Customer implements Serializable {

    @Id
    @Column(name="customer_id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int customerId;

    @Column(name="first_name")
    private String firstName;

    @Column(name="last_name")
    private String lastName;

    @Column(name="city")
    private String city;

    @Column(name="zip_code")
    private String zipCode;

    @Column(name = "date_joined")
    private LocalDate dateJoined;

    public Customer() {}

    public Customer(String firstName, String lastName, String city, String zipCode, LocalDate dateJoined) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.city = city;
        this.zipCode = zipCode;
        this.dateJoined = dateJoined;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public LocalDate getDateJoined() {
        return dateJoined;
    }

    public void setDateJoined(LocalDate dateJoined) {
        this.dateJoined = dateJoined;
    }

    @JoinColumn(name="customer_id", insertable = false, updatable = false, nullable = false)
    @OneToMany(cascade={CascadeType.MERGE, CascadeType.PERSIST})
    private List<Policy> policies = new ArrayList<Policy>();

    public List<Policy> getPolicies() {
        return policies;
    }

    public void setPolicy(List<Policy> policies) {
        this.policies = policies;
    }

}

package com.conygre.proj1.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name="policies")

public class Policy implements Serializable {

    //Instance variables

    @Id
    @Column(name="policy_id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer policyId;

    @Column(name="policy_type")
    private String policyType;

    @Column(name="added_date")
    private LocalDate addedDate;

    @Column(name="renewal_date")
    private LocalDate renewalDate;

    @Column(name="monthly_premium")
    private double monthlyPremium;

    @Column(name="customer_id")
    private int customerId;

    //Methods
    public Integer getPolicyId(){
        return policyId;
    }

    public void setPolicyId(Integer s){
        policyId = s;
    }

    public void setPolicyType(String s){
        policyType = s;
    }

    public String getPolicyType(){
        return policyType;
    }

    public LocalDate getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(LocalDate addedDate) {
        this.addedDate = addedDate;
    }

    public LocalDate getRenewalDate() {
        return renewalDate;
    }

    public void setRenewalDate(LocalDate renewalDate) {
        this.renewalDate = renewalDate;
    }

    public double getMonthlyPremium() {
        return monthlyPremium;
    }

    public void setMonthlyPremium(double monthlyPremium) {
        this.monthlyPremium = monthlyPremium;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    //constructors
    public Policy(){}

    public Policy(int policyId, String policyType, int customerId, LocalDate addedDate, LocalDate renewalDate, double monthlyPremium){
        this.policyType = policyType;
        this.policyId = policyId;
        this.customerId = customerId;
        this.addedDate = addedDate;
        this.renewalDate = renewalDate;
        this.monthlyPremium = monthlyPremium;

    }

    public Policy(String title) {
        this.policyType = policyType;
    }
}

package com.conygre.proj1.services;

import com.conygre.proj1.entities.Customer;

import java.util.Collection;


public interface CustomerService {
    Collection<Customer> getCustomers();
    Customer getCustomerById(int id);
    Customer updateCustomer(Customer updatedCustomer);

    Customer addNewCustomer(Customer newCustomer);
    public void deleteCustomer(Customer customer);
    public void deleteCustomer(int id);
}

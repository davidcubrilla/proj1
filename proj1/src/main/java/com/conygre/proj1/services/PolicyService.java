package com.conygre.proj1.services;

import com.conygre.proj1.entities.Policy;

import java.util.Collection;

public interface PolicyService {
    Collection<Policy> getPolicies();
    Policy getPolicyById(int id);
    Policy updatePolicy(Policy updatedPolicy);

    Policy addNewPolicy(Policy newPolicy);
    public void deletePolicy(Policy policy);
    public void deletePolicy(int id);
}

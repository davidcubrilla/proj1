package com.conygre.proj1.services;

import com.conygre.proj1.entities.Customer;
import com.conygre.proj1.repos.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    @Override
    public Collection<Customer> getCustomers() {
        return customerRepository.findAll();
    }

    @Override
    public Customer getCustomerById(int id) {
        return customerRepository.findById(id).get();
    }

    @Override
    public Customer updateCustomer(Customer updatedCustomer) {
        return customerRepository.save(updatedCustomer);
    }

    @Override
    public Customer addNewCustomer(Customer newCustomer) {
        return customerRepository.save(newCustomer);
    }

    @Override
    public void deleteCustomer(int id) {
        Customer toBeDeleted = customerRepository.findById(id).get();
        deleteCustomer(toBeDeleted);
    }
    @Override
    public void deleteCustomer(Customer customer) {
        customerRepository.delete(customer);
    }
}

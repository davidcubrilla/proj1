package com.conygre.proj1.services;

import com.conygre.proj1.entities.Policy;
import com.conygre.proj1.repos.PolicyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class PolicyServiceImpl implements PolicyService {

    @Autowired
    private PolicyRepository policyRepository;

    @Override
    public Collection<Policy> getPolicies() {
        return policyRepository.findAll();
    }

    @Override
    public Policy getPolicyById(int id) {
        return policyRepository.findById(id).get();
    }

    @Override
    public Policy updatePolicy(Policy updatedPolicy) {
        return policyRepository.save(updatedPolicy);
    }

    @Override
    public Policy addNewPolicy(Policy newPolicy) {
        return policyRepository.save(newPolicy);
    }

    @Override
    public void deletePolicy(int id) {
        Policy toBeDeleted = policyRepository.findById(id).get();
        deletePolicy(toBeDeleted);
    }
    @Override
    public void deletePolicy(Policy policy) {
        policyRepository.delete(policy);
    }
}

CREATE TABLE `customers` (
                             `customer_id` int NOT NULL AUTO_INCREMENT,
                             `first_name` varchar(45) NOT NULL,
                             `last_name` varchar(45) NOT NULL,
                             `city` varchar(45) NOT NULL,
                             `zip_code` varchar(45) NOT NULL,
                             `date_joined` date NOT NULL,
                             PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `policies` (
                            `policy_id` int NOT NULL AUTO_INCREMENT,
                            `customer_id` int NOT NULL,
                            `policy_type` varchar(45) NOT NULL,
                            `added_date` date NOT NULL,
                            `renewal_date` date NOT NULL,
                            `monthly_premium` decimal(10,0) NOT NULL,
                            PRIMARY KEY (`policy_id`),
                            KEY `customer_id_idx` (`customer_id`),
                            CONSTRAINT `customer_id` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;



INSERT INTO `customerpolicies`.`customers` (`customer_id`, `first_name`, `last_name`, `city`, `zip_code`, `date_joined`) VALUES ('1', 'David', 'Cubrilla', 'Charlotte', '28202', '2021-09-01');
INSERT INTO `customerpolicies`.`customers` (`customer_id`, `first_name`, `last_name`, `city`, `zip_code`, `date_joined`) VALUES ('2', 'Ryan', 'Collins', 'Charlotte', '28202', '2021-02-01');
INSERT INTO `customerpolicies`.`customers` (`customer_id`, `first_name`, `last_name`, `city`, `zip_code`, `date_joined`) VALUES ('3', 'Erin', 'Wilhjelm', 'Dallas', '70001', '2021-07-14');
INSERT INTO `customerpolicies`.`customers` (`customer_id`, `first_name`, `last_name`, `city`, `zip_code`, `date_joined`) VALUES ('4', 'Nick', 'Todd', 'London', '12345', '2019-03-17');


INSERT INTO `customerpolicies`.`policies` (`policy_id`, `customer_id`, `policy_type`, `added_date`, `renewal_date`, `monthly_premium`) VALUES ('1', '1', 'Collision', '2021-07-01', '2022-01-01', '102.50');
INSERT INTO `customerpolicies`.`policies` (`policy_id`, `customer_id`, `policy_type`, `added_date`, `renewal_date`, `monthly_premium`) VALUES ('2', '1', 'Medical', '2021-04-08', '2021-10-08', '148.99');
INSERT INTO `customerpolicies`.`policies` (`policy_id`, `customer_id`, `policy_type`, `added_date`, `renewal_date`, `monthly_premium`) VALUES ('3', '2', 'Collision', '2021-06-25', '2021-12-25', '108.12');
INSERT INTO `customerpolicies`.`policies` (`policy_id`, `customer_id`, `policy_type`, `added_date`, `renewal_date`, `monthly_premium`) VALUES ('4', '3', 'Collision', '2021-04-20', '2021-10-20', '98.20');
INSERT INTO `customerpolicies`.`policies` (`policy_id`, `customer_id`, `policy_type`, `added_date`, `renewal_date`, `monthly_premium`) VALUES ('5', '3', 'Medical', '2021-04-20', '2021-10-20', '130.97');
INSERT INTO `customerpolicies`.`policies` (`policy_id`, `customer_id`, `policy_type`, `added_date`, `renewal_date`, `monthly_premium`) VALUES ('6', '4', 'Liability', '2021-04-10', '2021-10-10', '68.75');
INSERT INTO `customerpolicies`.`policies` (`policy_id`, `customer_id`, `policy_type`, `added_date`, `renewal_date`, `monthly_premium`) VALUES ('7', '4', 'Roadside Coverage', '2021-04-10', '2021-10-10', '56.75');

